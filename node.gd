extends Spatial

var velocity = Vector3(0.0,0.0, 0.0)
var old_position = Vector3(0.0,0.0,0.0)
var mass = 1.0
# reciprocal mass mu = 1./mass
var mu = 1.0
var gravitational_acc = Vector3( 0.0, -0.0001, 0.0 )
var gravity = self.mass * gravitational_acc
var resistance = 0;

var static_node = false

# arrays with neighbours, as well as the corresponding constants
var neighbors = []
var bounce_coeff = [] #ks
var rest_lenght = [] #ls
var dumping_coeff = [] #kt
var stiffness_coeff = 0.5

# calulation of step_{-1}
func _ready():
	old_position = translation - velocity*get_physics_process_delta_time()

# static nodes do not change position and change color to blue
func change_to_static():
	static_node = true
	get_child(0).hide()
	get_child(1).show()
	
# non static nodes change position and change color to pink
func change_to_non_static():
	static_node = false
	get_child(1).hide()
	get_child(0).show()


# calculation of the force
func force():
#	if static_node == true :
#		return 0
	var force = Vector3()
	var stiffness_force = Vector3()
	
	for i in range(0, neighbors.size()-1):

		#force from Hooke's Law 
		var direction_12  = neighbors[i].translation - self.translation
		stiffness_force += direction_12
		var current_lenght = rest_lenght[i] - (direction_12.length())
		force -= bounce_coeff[i] * current_lenght * direction_12.normalized()
		
		#force of dumping
		var velocity_21 = self.velocity - neighbors[i].velocity
		force -= dumping_coeff[i] * velocity_21.dot(direction_12.normalized()) * direction_12.normalized()
		
	#force of stiffness
	force += stiffness_coeff * stiffness_force
	resistance = (-0.1) * self.velocity
	
	return force + resistance + gravity
	
func add_neighbor(node, lenght, bounce, dumping):
	neighbors.push_back(node)
	rest_lenght.push_back(lenght)
	bounce_coeff.push_back(bounce)
	dumping_coeff.push_back(dumping)

# using verlet algorithm
func _physics_process(delta):
	verlet(delta)

# smart setters
func set_velocity(v):
	velocity = v
	old_position = translation - velocity*get_physics_process_delta_time()

func set_mass(m):
	mass = m
	mu = pow(m,-1.0)

# integrating algorithms: euler and verlet
func euler(delta):
	velocity += force()*mu*delta
	translation += velocity*delta

func verlet(delta):
	var new_position = translation
	#only non static nodes change position
	if static_node == false :
		new_position = 2*translation - old_position + force()*mu*pow(delta,2.0)
	old_position = translation
	translation = new_position
	velocity = (translation - old_position)/delta
