extends Spatial

var node_object = load("res://node.tscn")
onready var im = get_node("../Draw");

var force          = Vector3()
var node_mass      = 2
onready var nodes = []

# size is a number of nodes in a row
var size = 4.0
# distance between nodes in particular axis
var dist_x = 1
var dist_y = 1
var dist_z = 1

var bounce_coeff = 1.0
var dumping_coeff = 2.0

var radius = sqrt(2);

# are the keys "→", "←", "↑" or "↓" pressed
var MOVE_LFT = false
var MOVE_RGT = false
var MOVE_UP  = false
var MOVE_DWN = false
var inf_DU = Vector3()
var inf_RL = Vector3()

func _ready():
	# spawning points
	for i in range(0,size):
		for j in range(0,size):
			for k in range(0,size):

				var new_node = node_object.instance()
				new_node.translation = Vector3( dist_x * j - size/3, dist_y * i - size/3, dist_z * k - size/3)
				new_node.rest_lenght.push_back(1.0)
				new_node.bounce_coeff.push_back(2.0)
				new_node.dumping_coeff.push_back(1.0)
				nodes.push_back(new_node)
				if (i == size-1 or i == 0) and (j == size-1 or j == 0) and (k == size-1 or k == 0):
#				if (i == 0) and (j == size-1 or j == 0) and (k == size-1 or k == 0):
#				if (i == 0):
					new_node.change_to_static()
				add_child(new_node)
				
	#adding neighbours
	for i in range(0, nodes.size()):
		for j in range(i, nodes.size()):
			if i != j :
				var distance = nodes[j].translation - nodes[i].translation
				distance = distance.length()
				if distance <= radius:
					nodes[i].add_neighbor(nodes[j], distance, bounce_coeff, dumping_coeff)
					nodes[j].add_neighbor(nodes[i], distance, bounce_coeff, dumping_coeff)
	pass
	
func draw_vector(from, to):
	im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
	im.add_vertex(from.translation)
	im.add_vertex(to.translation)
	im.end()

func _input(event):
	if event.is_action_pressed("ui_left"):
		MOVE_LFT = true
	if event.is_action_released("ui_left"):
		MOVE_LFT = false
	if event.is_action_pressed("ui_right"):
		MOVE_RGT = true
	if event.is_action_released("ui_right"):
		MOVE_RGT = false
	if event.is_action_pressed("ui_up"):
		MOVE_UP  = true
	if event.is_action_released("ui_up"):
		MOVE_UP  = false
	if event.is_action_pressed("ui_down"):
		MOVE_DWN = true
	if event.is_action_released("ui_down"):
		MOVE_DWN = false

func _process(delta):
	# clear drawing
	im.clear()
	# drawing connections
	for i in range(0, nodes.size()):
		for n in range(0, nodes[i].neighbors.size()):
			draw_vector(nodes[i], nodes[i].neighbors[n])
	
	if MOVE_LFT:
		nodes[0].position -= inf_RL
	if MOVE_RGT:
		nodes[0].position += inf_RL
	if MOVE_UP:
		nodes[0].position -= inf_DU
	if MOVE_DWN:
		nodes[0].position += inf_DU
	
